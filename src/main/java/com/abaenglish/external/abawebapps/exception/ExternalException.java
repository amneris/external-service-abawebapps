package com.abaenglish.external.abawebapps.exception;

import org.springframework.http.HttpStatus;

public abstract class ExternalException extends Exception {

    private static final long serialVersionUID = 4958689719277571957L;

    private HttpStatus httpStatus;

    public ExternalException(Exception ex, final HttpStatus status) {
        super(ex);
        this.httpStatus = status;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}

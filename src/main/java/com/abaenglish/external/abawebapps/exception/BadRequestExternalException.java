package com.abaenglish.external.abawebapps.exception;

import org.springframework.http.HttpStatus;

public class BadRequestExternalException extends ExternalException {

    public BadRequestExternalException(Exception ex) {
        super(ex, HttpStatus.BAD_REQUEST);
    }

}

package com.abaenglish.external.abawebapps.exception;

import org.springframework.http.HttpStatus;

public class UnKnownException extends ExternalException {

    public UnKnownException(Exception ex) {
        super(ex, HttpStatus.NOT_FOUND);
    }

}

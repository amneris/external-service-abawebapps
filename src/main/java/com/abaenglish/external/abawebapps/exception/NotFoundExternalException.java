package com.abaenglish.external.abawebapps.exception;

import org.springframework.http.HttpStatus;

public class NotFoundExternalException extends ExternalException {

    public NotFoundExternalException(Exception ex) {
        super(ex, HttpStatus.NOT_FOUND);
    }

}

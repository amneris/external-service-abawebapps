package com.abaenglish.external.abawebapps.autoconfigure;

import com.abaenglish.external.abawebapps.service.impl.AbawebappsExternalService;
import com.abaenglish.external.abawebapps.service.properties.AbawebappsProperties;
import org.springframework.context.annotation.Import;

@Import({AbawebappsProperties.class, AbawebappsExternalService.class})
public class ExternalServiceAbawebappsAutoConfiguration {
}

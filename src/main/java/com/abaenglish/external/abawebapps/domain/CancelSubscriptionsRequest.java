package com.abaenglish.external.abawebapps.domain;

public class CancelSubscriptionsRequest {

    private String originalSubscriptionId;
    private String accountId;
    private String cancelledDate;

    public String getOriginalSubscriptionId() {
        return originalSubscriptionId;
    }

    public void setOriginalSubscriptionId(String originalSubscriptionId) {
        this.originalSubscriptionId = originalSubscriptionId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(String cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public static final class Builder {
        private String originalSubscriptionId;
        private String accountId;
        private String cancelledDate;

        private Builder() {
        }

        public static Builder aCancelSubscriptionsRequest() {
            return new Builder();
        }

        public Builder originalSubscriptionId(String originalSubscriptionId) {
            this.originalSubscriptionId = originalSubscriptionId;
            return this;
        }

        public Builder accountId(String accountId) {
            this.accountId = accountId;
            return this;
        }

        public Builder cancelledDate(String cancelledDate) {
            this.cancelledDate = cancelledDate;
            return this;
        }

        public CancelSubscriptionsRequest build() {
            CancelSubscriptionsRequest cancelSubscriptionsRequest = new CancelSubscriptionsRequest();
            cancelSubscriptionsRequest.setOriginalSubscriptionId(originalSubscriptionId);
            cancelSubscriptionsRequest.setAccountId(accountId);
            cancelSubscriptionsRequest.setCancelledDate(cancelledDate);
            return cancelSubscriptionsRequest;
        }
    }
}

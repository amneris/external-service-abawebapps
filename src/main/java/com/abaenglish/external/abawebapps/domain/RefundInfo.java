package com.abaenglish.external.abawebapps.domain;

import java.math.BigDecimal;

public class RefundInfo {

    private String id;
    private String accountId;
    private BigDecimal amount;
    private String refundDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRefundDate() {
        return refundDate;
    }

    public void setRefundDate(String refundDate) {
        this.refundDate = refundDate;
    }

    public static final class Builder {
        private String id;
        private String accountId;
        private BigDecimal amount;
        private String refundDate;

        private Builder() {
        }

        public static Builder aRefundInfo() {
            return new Builder();
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder accountId(String accountId) {
            this.accountId = accountId;
            return this;
        }

        public Builder amount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public Builder refundDate(String refundDate) {
            this.refundDate = refundDate;
            return this;
        }

        public RefundInfo build() {
            RefundInfo refundInfo = new RefundInfo();
            refundInfo.setId(id);
            refundInfo.setAccountId(accountId);
            refundInfo.setAmount(amount);
            refundInfo.setRefundDate(refundDate);
            return refundInfo;
        }
    }
}

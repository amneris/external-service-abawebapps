package com.abaenglish.external.abawebapps.domain;

public class UserZuora {

    private Zuora zuora;
    private User user;

    public Zuora getZuora() {
        return zuora;
    }

    public void setZuora(Zuora zuora) {
        this.zuora = zuora;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public static final class Builder {
        private Zuora zuora;
        private User user;

        private Builder() {
        }

        public static Builder anUserZuora() {
            return new Builder();
        }

        public Builder zuora(Zuora zuora) {
            this.zuora = zuora;
            return this;
        }

        public Builder user(User user) {
            this.user = user;
            return this;
        }

        public UserZuora build() {
            UserZuora userZuora = new UserZuora();
            userZuora.setZuora(zuora);
            userZuora.setUser(user);
            return userZuora;
        }
    }
}

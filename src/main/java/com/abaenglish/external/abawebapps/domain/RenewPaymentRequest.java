package com.abaenglish.external.abawebapps.domain;

public class RenewPaymentRequest {

    private String originalSubscriptionId;
    private String paymentDate;
    private String nextRenewalDate;
    private String invoiceId;
    private String invoiceNumber;

    public String getOriginalSubscriptionId() {
        return originalSubscriptionId;
    }

    public void setOriginalSubscriptionId(String originalSubscriptionId) {
        this.originalSubscriptionId = originalSubscriptionId;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getNextRenewalDate() {
        return nextRenewalDate;
    }

    public void setNextRenewalDate(String nextRenewalDate) {
        this.nextRenewalDate = nextRenewalDate;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public static final class Builder {
        private String originalSubscriptionId;
        private String paymentDate;
        private String nextRenewalDate;
        private String invoiceId;
        private String invoiceNumber;

        private Builder() {
        }

        public static Builder aRenewPaymentRequest() {
            return new Builder();
        }

        public Builder originalSubscriptionId(String originalSubscriptionId) {
            this.originalSubscriptionId = originalSubscriptionId;
            return this;
        }

        public Builder paymentDate(String paymentDate) {
            this.paymentDate = paymentDate;
            return this;
        }

        public Builder nextRenewalDate(String nextRenewalDate) {
            this.nextRenewalDate = nextRenewalDate;
            return this;
        }

        public Builder invoiceId(String invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        public Builder invoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public RenewPaymentRequest build() {
            RenewPaymentRequest renewPaymentRequest = new RenewPaymentRequest();
            renewPaymentRequest.setOriginalSubscriptionId(originalSubscriptionId);
            renewPaymentRequest.setPaymentDate(paymentDate);
            renewPaymentRequest.setNextRenewalDate(nextRenewalDate);
            renewPaymentRequest.setInvoiceId(invoiceId);
            renewPaymentRequest.setInvoiceNumber(invoiceNumber);
            return renewPaymentRequest;
        }
    }
}

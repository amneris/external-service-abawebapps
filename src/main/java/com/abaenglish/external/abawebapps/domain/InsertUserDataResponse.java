package com.abaenglish.external.abawebapps.domain;

public class InsertUserDataResponse {

    private Integer httpCode;
    private Integer httpCodePayment;
    private Integer httpCodeSubscription;
    private String paymentId;
    private String partnerId;
    private String message;

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public Integer getHttpCodePayment() {
        return httpCodePayment;
    }

    public void setHttpCodePayment(Integer httpCodePayment) {
        this.httpCodePayment = httpCodePayment;
    }

    public Integer getHttpCodeSubscription() {
        return httpCodeSubscription;
    }

    public void setHttpCodeSubscription(Integer httpCodeSubscription) {
        this.httpCodeSubscription = httpCodeSubscription;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static final class Builder {
        private Integer httpCode;
        private Integer httpCodePayment;
        private Integer httpCodeSubscription;
        private String paymentId;
        private String partnerId;
        private String message;

        private Builder() {
        }

        public static Builder anInsertUserDataResponse() {
            return new Builder();
        }

        public Builder httpCode(Integer httpCode) {
            this.httpCode = httpCode;
            return this;
        }

        public Builder httpCodePayment(Integer httpCodePayment) {
            this.httpCodePayment = httpCodePayment;
            return this;
        }

        public Builder httpCodeSubscription(Integer httpCodeSubscription) {
            this.httpCodeSubscription = httpCodeSubscription;
            return this;
        }

        public Builder paymentId(String paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public Builder partnerId(String partnerId) {
            this.partnerId = partnerId;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public InsertUserDataResponse build() {
            InsertUserDataResponse insertUserDataResponse = new InsertUserDataResponse();
            insertUserDataResponse.setHttpCode(httpCode);
            insertUserDataResponse.setHttpCodePayment(httpCodePayment);
            insertUserDataResponse.setHttpCodeSubscription(httpCodeSubscription);
            insertUserDataResponse.setPaymentId(paymentId);
            insertUserDataResponse.setPartnerId(partnerId);
            insertUserDataResponse.setMessage(message);
            return insertUserDataResponse;
        }
    }
}

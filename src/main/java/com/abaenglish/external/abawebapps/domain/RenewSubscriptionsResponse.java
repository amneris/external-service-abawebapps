package com.abaenglish.external.abawebapps.domain;

import java.util.ArrayList;
import java.util.List;

public class RenewSubscriptionsResponse {

    List<SubscriptionInfoResponse> success = new ArrayList<>();

    public List<SubscriptionInfoResponse> getSuccess() {
        return success;
    }

    public void setSuccess(List<SubscriptionInfoResponse> success) {
        this.success = success;
    }

    public static final class Builder {
        List<SubscriptionInfoResponse> success = new ArrayList<>();

        private Builder() {
        }

        public static Builder aRenewSubscriptionsResponse() {
            return new Builder();
        }

        public Builder success(List<SubscriptionInfoResponse> success) {
            this.success = success;
            return this;
        }

        public RenewSubscriptionsResponse build() {
            RenewSubscriptionsResponse renewSubscriptionsResponse = new RenewSubscriptionsResponse();
            renewSubscriptionsResponse.setSuccess(success);
            return renewSubscriptionsResponse;
        }
    }
}

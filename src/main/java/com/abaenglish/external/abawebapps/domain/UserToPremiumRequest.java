package com.abaenglish.external.abawebapps.domain;

public class UserToPremiumRequest {

    private Long userId;
    private String expirationDate;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public static final class Builder {
        private Long userId;
        private String expirationDate;

        private Builder() {
        }

        public static Builder anUserToPremiumRequest() {
            return new Builder();
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder expirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public UserToPremiumRequest build() {
            UserToPremiumRequest userToPremiumRequest = new UserToPremiumRequest();
            userToPremiumRequest.setUserId(userId);
            userToPremiumRequest.setExpirationDate(expirationDate);
            return userToPremiumRequest;
        }
    }
}

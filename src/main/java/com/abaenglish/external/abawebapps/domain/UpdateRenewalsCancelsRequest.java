package com.abaenglish.external.abawebapps.domain;

public class UpdateRenewalsCancelsRequest {

    private Long userId;
    private String expirationDate;
    private String userType;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public static final class Builder {
        private Long userId;
        private String expirationDate;
        private String userType;

        private Builder() {
        }

        public static Builder anUpdateRenewalsCancelsRequest() {
            return new Builder();
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder expirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public Builder userType(String userType) {
            this.userType = userType;
            return this;
        }

        public UpdateRenewalsCancelsRequest build() {
            UpdateRenewalsCancelsRequest updateRenewalsCancelsRequest = new UpdateRenewalsCancelsRequest();
            updateRenewalsCancelsRequest.setUserId(userId);
            updateRenewalsCancelsRequest.setExpirationDate(expirationDate);
            updateRenewalsCancelsRequest.setUserType(userType);
            return updateRenewalsCancelsRequest;
        }
    }
}

package com.abaenglish.external.abawebapps.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CountryIso {

    @JsonProperty("iso3")
    private String abaCountryIso;

    private Long idCountry;

    public String getAbaCountryIso() {
        return abaCountryIso;
    }

    public void setAbaCountryIso(String abaCountryIso) {
        this.abaCountryIso = abaCountryIso;
    }

    public Long getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(Long idCountry) {
        this.idCountry = idCountry;
    }

    public static final class Builder {
        private String abaCountryIso;
        private Long idCountry;

        private Builder() {
        }

        public static Builder aCountryIso() {
            return new Builder();
        }

        public Builder abaCountryIso(String abaCountryIso) {
            this.abaCountryIso = abaCountryIso;
            return this;
        }

        public Builder idCountry(Long idCountry) {
            this.idCountry = idCountry;
            return this;
        }

        public CountryIso build() {
            CountryIso countryIso = new CountryIso();
            countryIso.setAbaCountryIso(abaCountryIso);
            countryIso.setIdCountry(idCountry);
            return countryIso;
        }
    }
}


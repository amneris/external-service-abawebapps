package com.abaenglish.external.abawebapps.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InsertUserRequest {

    private Long userId;
    private String zuoraAccountId;
    private String zuoraAccountNumber;

    private Integer periodId;
    private String currency;
    private BigDecimal price;
    private String idCountry;

    private String idDiscount;
    private String idRatePlanCharge;

    private String invoiceId;
    private String invoiceNumber;
    private BigDecimal finalPrice;

    private String subscriptionId;
    private String subscriptionNumber;

    private List<SubscriptionAbawebappsInfo> subscriptionsList = new ArrayList<>();

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getZuoraAccountId() {
        return zuoraAccountId;
    }

    public void setZuoraAccountId(String zuoraAccountId) {
        this.zuoraAccountId = zuoraAccountId;
    }

    public String getZuoraAccountNumber() {
        return zuoraAccountNumber;
    }

    public void setZuoraAccountNumber(String zuoraAccountNumber) {
        this.zuoraAccountNumber = zuoraAccountNumber;
    }

    public Integer getPeriodId() {
        return periodId;
    }

    public void setPeriodId(Integer periodId) {
        this.periodId = periodId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(String idCountry) {
        this.idCountry = idCountry;
    }

    public String getIdDiscount() {
        return idDiscount;
    }

    public void setIdDiscount(String idDiscount) {
        this.idDiscount = idDiscount;
    }

    public String getIdRatePlanCharge() {
        return idRatePlanCharge;
    }

    public void setIdRatePlanCharge(String idRatePlanCharge) {
        this.idRatePlanCharge = idRatePlanCharge;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionNumber() {
        return subscriptionNumber;
    }

    public void setSubscriptionNumber(String subscriptionNumber) {
        this.subscriptionNumber = subscriptionNumber;
    }

    public List<SubscriptionAbawebappsInfo> getSubscriptionsList() {
        return subscriptionsList;
    }

    public void setSubscriptionsList(List<SubscriptionAbawebappsInfo> subscriptionsList) {
        this.subscriptionsList = subscriptionsList;
    }

    public static final class Builder {
        private Long userId;
        private String zuoraAccountId;
        private String zuoraAccountNumber;
        private Integer periodId;
        private String currency;
        private BigDecimal price;
        private String idCountry;
        private String idDiscount;
        private String idRatePlanCharge;
        private String invoiceId;
        private String invoiceNumber;
        private BigDecimal finalPrice;
        private String subscriptionId;
        private String subscriptionNumber;
        private List<SubscriptionAbawebappsInfo> subscriptionsList = new ArrayList<>();

        private Builder() {
        }

        public static Builder anInsertUserRequest() {
            return new Builder();
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder zuoraAccountId(String zuoraAccountId) {
            this.zuoraAccountId = zuoraAccountId;
            return this;
        }

        public Builder zuoraAccountNumber(String zuoraAccountNumber) {
            this.zuoraAccountNumber = zuoraAccountNumber;
            return this;
        }

        public Builder periodId(Integer periodId) {
            this.periodId = periodId;
            return this;
        }

        public Builder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public Builder price(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder idCountry(String idCountry) {
            this.idCountry = idCountry;
            return this;
        }

        public Builder idDiscount(String idDiscount) {
            this.idDiscount = idDiscount;
            return this;
        }

        public Builder idRatePlanCharge(String idRatePlanCharge) {
            this.idRatePlanCharge = idRatePlanCharge;
            return this;
        }

        public Builder invoiceId(String invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        public Builder invoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public Builder finalPrice(BigDecimal finalPrice) {
            this.finalPrice = finalPrice;
            return this;
        }

        public Builder subscriptionId(String subscriptionId) {
            this.subscriptionId = subscriptionId;
            return this;
        }

        public Builder subscriptionNumber(String subscriptionNumber) {
            this.subscriptionNumber = subscriptionNumber;
            return this;
        }

        public Builder subscriptionsList(List<SubscriptionAbawebappsInfo> subscriptionsList) {
            this.subscriptionsList = subscriptionsList;
            return this;
        }

        public InsertUserRequest build() {
            InsertUserRequest insertUserRequest = new InsertUserRequest();
            insertUserRequest.setUserId(userId);
            insertUserRequest.setZuoraAccountId(zuoraAccountId);
            insertUserRequest.setZuoraAccountNumber(zuoraAccountNumber);
            insertUserRequest.setPeriodId(periodId);
            insertUserRequest.setCurrency(currency);
            insertUserRequest.setPrice(price);
            insertUserRequest.setIdCountry(idCountry);
            insertUserRequest.setIdDiscount(idDiscount);
            insertUserRequest.setIdRatePlanCharge(idRatePlanCharge);
            insertUserRequest.setInvoiceId(invoiceId);
            insertUserRequest.setInvoiceNumber(invoiceNumber);
            insertUserRequest.setFinalPrice(finalPrice);
            insertUserRequest.setSubscriptionId(subscriptionId);
            insertUserRequest.setSubscriptionNumber(subscriptionNumber);
            insertUserRequest.setSubscriptionsList(subscriptionsList);
            return insertUserRequest;
        }
    }
}

package com.abaenglish.external.abawebapps.domain;

public class UpdateInvoiceResponse {

    private Boolean success;
    private String paymentId;
    private String partnerId;
    private String warnings;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getWarnings() {
        return warnings;
    }

    public void setWarnings(String warnings) {
        this.warnings = warnings;
    }

    public static final class Builder {
        private Boolean success;
        private Boolean successFreeToPremium;
        private Boolean successSubscription;
        private String paymentId;
        private String partnerId;
        private String warnings;

        private Builder() {
        }

        public static Builder anInsertUserResponse() {
            return new Builder();
        }

        public Builder success(Boolean success) {
            this.success = success;
            return this;
        }

        public Builder successFreeToPremium(Boolean successFreeToPremium) {
            this.successFreeToPremium = successFreeToPremium;
            return this;
        }

        public Builder successSubscription(Boolean successSubscription) {
            this.successSubscription = successSubscription;
            return this;
        }

        public Builder paymentId(String paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public Builder partnerId(String partnerId) {
            this.partnerId = partnerId;
            return this;
        }

        public Builder warnings(String warnings) {
            this.warnings = warnings;
            return this;
        }

        public UpdateInvoiceResponse build() {
            UpdateInvoiceResponse insertUserResponse = new UpdateInvoiceResponse();
            insertUserResponse.setSuccess(success);
            insertUserResponse.setPaymentId(paymentId);
            insertUserResponse.setPartnerId(partnerId);
            insertUserResponse.setWarnings(warnings);
            return insertUserResponse;
        }
    }
}

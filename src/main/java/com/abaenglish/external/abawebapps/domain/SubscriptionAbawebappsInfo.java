package com.abaenglish.external.abawebapps.domain;

public class SubscriptionAbawebappsInfo {

    private String id;
    private String accountId;
    private String termStartDate;
    private String termEndDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getTermStartDate() {
        return termStartDate;
    }

    public void setTermStartDate(String termStartDate) {
        this.termStartDate = termStartDate;
    }

    public String getTermEndDate() {
        return termEndDate;
    }

    public void setTermEndDate(String termEndDate) {
        this.termEndDate = termEndDate;
    }

    public static final class Builder {
        private String id;
        private String accountId;
        private String termStartDate;
        private String termEndDate;

        private Builder() {
        }

        public static Builder aSubscriptionAbawebappsInfo() {
            return new Builder();
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder accountId(String accountId) {
            this.accountId = accountId;
            return this;
        }

        public Builder termStartDate(String termStartDate) {
            this.termStartDate = termStartDate;
            return this;
        }

        public Builder termEndDate(String termEndDate) {
            this.termEndDate = termEndDate;
            return this;
        }

        public SubscriptionAbawebappsInfo build() {
            SubscriptionAbawebappsInfo subscriptionAbawebappsInfo = new SubscriptionAbawebappsInfo();
            subscriptionAbawebappsInfo.setId(id);
            subscriptionAbawebappsInfo.setAccountId(accountId);
            subscriptionAbawebappsInfo.setTermStartDate(termStartDate);
            subscriptionAbawebappsInfo.setTermEndDate(termEndDate);
            return subscriptionAbawebappsInfo;
        }
    }
}

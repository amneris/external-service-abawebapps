package com.abaenglish.external.abawebapps.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Currency {

    @JsonProperty("ABAIdCurrency")
    private String abaIdCurrency;

    public String getAbaIdCurrency() {
        return abaIdCurrency;
    }

    public void setAbaIdCurrency(String abaIdCurrency) {
        this.abaIdCurrency = abaIdCurrency;
    }

    public static final class Builder {
        private String abaIdCurrency;

        private Builder() {
        }

        public static Builder aCurrency() {
            return new Builder();
        }

        public Builder abaIdCurrency(String abaIdCurrency) {
            this.abaIdCurrency = abaIdCurrency;
            return this;
        }

        public Currency build() {
            Currency currency = new Currency();
            currency.setAbaIdCurrency(abaIdCurrency);
            return currency;
        }
    }

}

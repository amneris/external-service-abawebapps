
package com.abaenglish.external.abawebapps.domain;


public class CurrencyISO {

    private String extCode;
    private String description;
    private String symbol;
    private String unitEur;
    private String outEur;
    private String unitUsd;
    private String outUsd;
    private String decimalPoint;
    private String dateLastUpdate;
    private String id;

    public String getExtCode() {
        return extCode;
    }

    public void setExtCode(String extCode) {
        this.extCode = extCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getUnitEur() {
        return unitEur;
    }

    public void setUnitEur(String unitEur) {
        this.unitEur = unitEur;
    }

    public String getOutEur() {
        return outEur;
    }

    public void setOutEur(String outEur) {
        this.outEur = outEur;
    }

    public String getUnitUsd() {
        return unitUsd;
    }

    public void setUnitUsd(String unitUsd) {
        this.unitUsd = unitUsd;
    }

    public String getOutUsd() {
        return outUsd;
    }

    public void setOutUsd(String outUsd) {
        this.outUsd = outUsd;
    }

    public String getDecimalPoint() {
        return decimalPoint;
    }

    public void setDecimalPoint(String decimalPoint) {
        this.decimalPoint = decimalPoint;
    }

    public String getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(String dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static final class Builder {
        private String extCode;
        private String description;
        private String symbol;
        private String unitEur;
        private String outEur;
        private String unitUsd;
        private String outUsd;
        private String decimalPoint;
        private String dateLastUpdate;
        private String id;

        private Builder() {
        }

        public static Builder aCurrencyISO() {
            return new Builder();
        }

        public Builder extCode(String extCode) {
            this.extCode = extCode;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder symbol(String symbol) {
            this.symbol = symbol;
            return this;
        }

        public Builder unitEur(String unitEur) {
            this.unitEur = unitEur;
            return this;
        }

        public Builder outEur(String outEur) {
            this.outEur = outEur;
            return this;
        }

        public Builder unitUsd(String unitUsd) {
            this.unitUsd = unitUsd;
            return this;
        }

        public Builder outUsd(String outUsd) {
            this.outUsd = outUsd;
            return this;
        }

        public Builder decimalPoint(String decimalPoint) {
            this.decimalPoint = decimalPoint;
            return this;
        }

        public Builder dateLastUpdate(String dateLastUpdate) {
            this.dateLastUpdate = dateLastUpdate;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public CurrencyISO build() {
            CurrencyISO currencyISO = new CurrencyISO();
            currencyISO.setExtCode(extCode);
            currencyISO.setDescription(description);
            currencyISO.setSymbol(symbol);
            currencyISO.setUnitEur(unitEur);
            currencyISO.setOutEur(outEur);
            currencyISO.setUnitUsd(unitUsd);
            currencyISO.setOutUsd(outUsd);
            currencyISO.setDecimalPoint(decimalPoint);
            currencyISO.setDateLastUpdate(dateLastUpdate);
            currencyISO.setId(id);
            return currencyISO;
        }
    }
}

package com.abaenglish.external.abawebapps.domain;

public class InsertUserResponse {

    private Boolean success;
    private Boolean successFreeToPremium;
    private Boolean successSubscription;
    private String paymentId;
    private String partnerId;
    private String warnings;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Boolean getSuccessFreeToPremium() {
        return successFreeToPremium;
    }

    public void setSuccessFreeToPremium(Boolean successFreeToPremium) {
        this.successFreeToPremium = successFreeToPremium;
    }

    public Boolean getSuccessSubscription() {
        return successSubscription;
    }

    public void setSuccessSubscription(Boolean successSubscription) {
        this.successSubscription = successSubscription;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getWarnings() {
        return warnings;
    }

    public void setWarnings(String warnings) {
        this.warnings = warnings;
    }

    public static final class Builder {
        private Boolean success;
        private Boolean successFreeToPremium;
        private Boolean successSubscription;
        private String paymentId;
        private String partnerId;
        private String warnings;

        private Builder() {
        }

        public static Builder anInsertUserResponse() {
            return new Builder();
        }

        public Builder success(Boolean success) {
            this.success = success;
            return this;
        }

        public Builder successFreeToPremium(Boolean successFreeToPremium) {
            this.successFreeToPremium = successFreeToPremium;
            return this;
        }

        public Builder successSubscription(Boolean successSubscription) {
            this.successSubscription = successSubscription;
            return this;
        }

        public Builder paymentId(String paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public Builder partnerId(String partnerId) {
            this.partnerId = partnerId;
            return this;
        }

        public Builder warnings(String warnings) {
            this.warnings = warnings;
            return this;
        }

        public InsertUserResponse build() {
            InsertUserResponse insertUserResponse = new InsertUserResponse();
            insertUserResponse.setSuccess(success);
            insertUserResponse.setSuccessFreeToPremium(successFreeToPremium);
            insertUserResponse.setSuccessSubscription(successSubscription);
            insertUserResponse.setPaymentId(paymentId);
            insertUserResponse.setPartnerId(partnerId);
            insertUserResponse.setWarnings(warnings);
            return insertUserResponse;
        }
    }
}

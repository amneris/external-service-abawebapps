package com.abaenglish.external.abawebapps.domain;

import java.util.ArrayList;
import java.util.List;

public class CancelSubscriptionsResponse {

    List<SubscriptionInfoResponse> success = new ArrayList<>();

    public List<SubscriptionInfoResponse> getSuccess() {
        return success;
    }

    public void setSuccess(List<SubscriptionInfoResponse> success) {
        this.success = success;
    }

    public static final class Builder {
        List<SubscriptionInfoResponse> success = new ArrayList<>();

        private Builder() {
        }

        public static Builder aCancelSubscriptionsResponse() {
            return new Builder();
        }

        public Builder success(List<SubscriptionInfoResponse> success) {
            this.success = success;
            return this;
        }

        public CancelSubscriptionsResponse build() {
            CancelSubscriptionsResponse cancelSubscriptionsResponse = new CancelSubscriptionsResponse();
            cancelSubscriptionsResponse.setSuccess(success);
            return cancelSubscriptionsResponse;
        }
    }

}

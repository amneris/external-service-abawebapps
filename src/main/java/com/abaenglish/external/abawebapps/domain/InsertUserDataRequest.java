package com.abaenglish.external.abawebapps.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InsertUserDataRequest {

    private Long userId;
    private String zuoraAccountId;
    private String zuoraAccountNumber;
    private String subscriptionId;
    private String subscriptionNumber;
    private String invoiceId;
    private String invoiceNumber;

    private List<SubscriptionAbawebappsInfo> subscriptionsList = new ArrayList<>();

    private Integer periodId;
    private String currency;
    private BigDecimal price;
    private String idCountry;
    private String idDiscount;
    private BigDecimal finalPrice;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getZuoraAccountId() {
        return zuoraAccountId;
    }

    public void setZuoraAccountId(String zuoraAccountId) {
        this.zuoraAccountId = zuoraAccountId;
    }

    public String getZuoraAccountNumber() {
        return zuoraAccountNumber;
    }

    public void setZuoraAccountNumber(String zuoraAccountNumber) {
        this.zuoraAccountNumber = zuoraAccountNumber;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionNumber() {
        return subscriptionNumber;
    }

    public void setSubscriptionNumber(String subscriptionNumber) {
        this.subscriptionNumber = subscriptionNumber;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public List<SubscriptionAbawebappsInfo> getSubscriptionsList() {
        return subscriptionsList;
    }

    public void setSubscriptionsList(List<SubscriptionAbawebappsInfo> subscriptionsList) {
        this.subscriptionsList = subscriptionsList;
    }

    public Integer getPeriodId() {
        return periodId;
    }

    public void setPeriodId(Integer periodId) {
        this.periodId = periodId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(String idCountry) {
        this.idCountry = idCountry;
    }

    public String getIdDiscount() {
        return idDiscount;
    }

    public void setIdDiscount(String idDiscount) {
        this.idDiscount = idDiscount;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public static final class Builder {
        private Long userId;
        private String zuoraAccountId;
        private String zuoraAccountNumber;
        private String subscriptionId;
        private String subscriptionNumber;
        private String invoiceId;
        private String invoiceNumber;
        private List<SubscriptionAbawebappsInfo> subscriptionsList = new ArrayList<>();
        private Integer periodId;
        private String currency;
        private BigDecimal price;
        private String idCountry;
        private String idDiscount;
        private BigDecimal finalPrice;

        private Builder() {
        }

        public static Builder anInsertUserDataRequest() {
            return new Builder();
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder zuoraAccountId(String zuoraAccountId) {
            this.zuoraAccountId = zuoraAccountId;
            return this;
        }

        public Builder zuoraAccountNumber(String zuoraAccountNumber) {
            this.zuoraAccountNumber = zuoraAccountNumber;
            return this;
        }

        public Builder subscriptionId(String subscriptionId) {
            this.subscriptionId = subscriptionId;
            return this;
        }

        public Builder subscriptionNumber(String subscriptionNumber) {
            this.subscriptionNumber = subscriptionNumber;
            return this;
        }

        public Builder invoiceId(String invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        public Builder invoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public Builder subscriptionsList(List<SubscriptionAbawebappsInfo> subscriptionsList) {
            this.subscriptionsList = subscriptionsList;
            return this;
        }

        public Builder periodId(Integer periodId) {
            this.periodId = periodId;
            return this;
        }

        public Builder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public Builder price(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder idCountry(String idCountry) {
            this.idCountry = idCountry;
            return this;
        }

        public Builder idDiscount(String idDiscount) {
            this.idDiscount = idDiscount;
            return this;
        }

        public Builder finalPrice(BigDecimal finalPrice) {
            this.finalPrice = finalPrice;
            return this;
        }

        public InsertUserDataRequest build() {
            InsertUserDataRequest insertUserDataRequest = new InsertUserDataRequest();
            insertUserDataRequest.setUserId(userId);
            insertUserDataRequest.setZuoraAccountId(zuoraAccountId);
            insertUserDataRequest.setZuoraAccountNumber(zuoraAccountNumber);
            insertUserDataRequest.setSubscriptionId(subscriptionId);
            insertUserDataRequest.setSubscriptionNumber(subscriptionNumber);
            insertUserDataRequest.setInvoiceId(invoiceId);
            insertUserDataRequest.setInvoiceNumber(invoiceNumber);
            insertUserDataRequest.setSubscriptionsList(subscriptionsList);
            insertUserDataRequest.setPeriodId(periodId);
            insertUserDataRequest.setCurrency(currency);
            insertUserDataRequest.setPrice(price);
            insertUserDataRequest.setIdCountry(idCountry);
            insertUserDataRequest.setIdDiscount(idDiscount);
            insertUserDataRequest.setFinalPrice(finalPrice);
            return insertUserDataRequest;
        }
    }
}

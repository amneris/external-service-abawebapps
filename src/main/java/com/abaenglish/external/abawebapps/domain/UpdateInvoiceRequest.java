package com.abaenglish.external.abawebapps.domain;

import java.math.BigDecimal;

public class UpdateInvoiceRequest {

    private String invoiceId;
    private String invoiceNumber;
    private BigDecimal totalAmount;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public static final class Builder {
        private String invoiceId;
        private String invoiceNumber;
        private BigDecimal totalAmount;

        private Builder() {
        }

        public static Builder anUpdateInvoiceRequest() {
            return new Builder();
        }

        public Builder invoiceId(String invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        public Builder invoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public Builder totalAmount(BigDecimal totalAmount) {
            this.totalAmount = totalAmount;
            return this;
        }

        public UpdateInvoiceRequest build() {
            UpdateInvoiceRequest updateInvoiceRequest = new UpdateInvoiceRequest();
            updateInvoiceRequest.setInvoiceId(invoiceId);
            updateInvoiceRequest.setInvoiceNumber(invoiceNumber);
            updateInvoiceRequest.setTotalAmount(totalAmount);
            return updateInvoiceRequest;
        }
    }

}

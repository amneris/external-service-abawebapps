package com.abaenglish.external.abawebapps.domain;

public class Zuora {

    private String zuoraAccountId;
    private String status;
    private String zuoraAccountNumber;
    private String dateAdd;

    public String getZuoraAccountId() {
        return zuoraAccountId;
    }

    public void setZuoraAccountId(String zuoraAccountId) {
        this.zuoraAccountId = zuoraAccountId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getZuoraAccountNumber() {
        return zuoraAccountNumber;
    }

    public void setZuoraAccountNumber(String zuoraAccountNumber) {
        this.zuoraAccountNumber = zuoraAccountNumber;
    }

    public String getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(String dateAdd) {
        this.dateAdd = dateAdd;
    }

    public static final class Builder {
        private String zuoraAccountId;
        private String status;
        private String zuoraAccountNumber;
        private String dateAdd;

        private Builder() {
        }

        public static Builder aZuora() {
            return new Builder();
        }

        public Builder zuoraAccountId(String zuoraAccountId) {
            this.zuoraAccountId = zuoraAccountId;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder zuoraAccountNumber(String zuoraAccountNumber) {
            this.zuoraAccountNumber = zuoraAccountNumber;
            return this;
        }

        public Builder dateAdd(String dateAdd) {
            this.dateAdd = dateAdd;
            return this;
        }

        public Zuora build() {
            Zuora zuora = new Zuora();
            zuora.setZuoraAccountId(zuoraAccountId);
            zuora.setStatus(status);
            zuora.setZuoraAccountNumber(zuoraAccountNumber);
            zuora.setDateAdd(dateAdd);
            return zuora;
        }
    }
}

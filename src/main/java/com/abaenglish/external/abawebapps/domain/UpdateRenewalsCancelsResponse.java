package com.abaenglish.external.abawebapps.domain;

public class UpdateRenewalsCancelsResponse {

    private Integer httpCode;
    private String message;

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static final class Builder {
        private Integer httpCode;
        private String message;

        private Builder() {
        }

        public static Builder anUserToPremiumResponse() {
            return new Builder();
        }

        public Builder httpCode(Integer httpCode) {
            this.httpCode = httpCode;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public UpdateRenewalsCancelsResponse build() {
            UpdateRenewalsCancelsResponse userToPremiumResponse = new UpdateRenewalsCancelsResponse();
            userToPremiumResponse.setHttpCode(httpCode);
            userToPremiumResponse.setMessage(message);
            return userToPremiumResponse;
        }
    }
}

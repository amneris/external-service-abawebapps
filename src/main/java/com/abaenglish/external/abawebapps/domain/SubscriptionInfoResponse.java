package com.abaenglish.external.abawebapps.domain;

public class SubscriptionInfoResponse {

    private String originalSubscriptionId;
    private Boolean success;
    private String warnings;

    public String getOriginalSubscriptionId() {
        return originalSubscriptionId;
    }

    public void setOriginalSubscriptionId(String originalSubscriptionId) {
        this.originalSubscriptionId = originalSubscriptionId;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getWarnings() {
        return warnings;
    }

    public void setWarnings(String warnings) {
        this.warnings = warnings;
    }

    public static final class Builder {
        private String originalSubscriptionId;
        private Boolean success;
        private String warnings;

        private Builder() {
        }

        public static Builder aSubscriptionInfoResponse() {
            return new Builder();
        }

        public Builder originalSubscriptionId(String originalSubscriptionId) {
            this.originalSubscriptionId = originalSubscriptionId;
            return this;
        }

        public Builder success(Boolean success) {
            this.success = success;
            return this;
        }

        public Builder warnings(String warnings) {
            this.warnings = warnings;
            return this;
        }

        public SubscriptionInfoResponse build() {
            SubscriptionInfoResponse subscriptionInfoResponse = new SubscriptionInfoResponse();
            subscriptionInfoResponse.setOriginalSubscriptionId(originalSubscriptionId);
            subscriptionInfoResponse.setSuccess(success);
            subscriptionInfoResponse.setWarnings(warnings);
            return subscriptionInfoResponse;
        }
    }
}

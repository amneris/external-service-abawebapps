package com.abaenglish.external.abawebapps.domain;

public class RenewSubscriptionsRequest {

    private String originalSubscriptionId;
    private String userExpirationDate;

    public String getOriginalSubscriptionId() {
        return originalSubscriptionId;
    }

    public void setOriginalSubscriptionId(String originalSubscriptionId) {
        this.originalSubscriptionId = originalSubscriptionId;
    }

    public String getUserExpirationDate() {
        return userExpirationDate;
    }

    public void setUserExpirationDate(String userExpirationDate) {
        this.userExpirationDate = userExpirationDate;
    }

    public static final class Builder {
        private String originalSubscriptionId;
        private String userExpirationDate;

        private Builder() {
        }

        public static Builder aRenewSubscriptionsRequest() {
            return new Builder();
        }

        public Builder originalSubscriptionId(String originalSubscriptionId) {
            this.originalSubscriptionId = originalSubscriptionId;
            return this;
        }

        public Builder userExpirationDate(String userExpirationDate) {
            this.userExpirationDate = userExpirationDate;
            return this;
        }

        public RenewSubscriptionsRequest build() {
            RenewSubscriptionsRequest renewSubscriptionsRequest = new RenewSubscriptionsRequest();
            renewSubscriptionsRequest.setOriginalSubscriptionId(originalSubscriptionId);
            renewSubscriptionsRequest.setUserExpirationDate(userExpirationDate);
            return renewSubscriptionsRequest;
        }
    }
}

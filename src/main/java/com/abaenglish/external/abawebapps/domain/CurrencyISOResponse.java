
package com.abaenglish.external.abawebapps.domain;


public class CurrencyISOResponse {

    private Integer httpCode;
    private String message;
    private CurrencyISO currencyISO;

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CurrencyISO getCurrencyISO() {
        return currencyISO;
    }

    public void setCurrencyISO(CurrencyISO currencyISO) {
        this.currencyISO = currencyISO;
    }

    public static final class Builder {
        private Integer httpCode;
        private String message;
        private CurrencyISO currencyISO;

        private Builder() {
        }

        public static Builder aCurrencyISOResponse() {
            return new Builder();
        }

        public Builder httpCode(Integer httpCode) {
            this.httpCode = httpCode;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder currencyISO(CurrencyISO currencyISO) {
            this.currencyISO = currencyISO;
            return this;
        }

        public CurrencyISOResponse build() {
            CurrencyISOResponse currencyISOResponse = new CurrencyISOResponse();
            currencyISOResponse.setHttpCode(httpCode);
            currencyISOResponse.setMessage(message);
            currencyISOResponse.setCurrencyISO(currencyISO);
            return currencyISOResponse;
        }
    }
}

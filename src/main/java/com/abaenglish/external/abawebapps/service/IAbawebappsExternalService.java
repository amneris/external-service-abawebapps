package com.abaenglish.external.abawebapps.service;

import com.abaenglish.external.abawebapps.domain.*;
import com.abaenglish.external.abawebapps.exception.ExternalException;
import com.abaenglish.external.abawebapps.exception.NotFoundExternalException;

import java.util.List;

public interface IAbawebappsExternalService {

    CurrencyISO getCurrencyISO (String iso) throws ExternalException;

    CountryIso getCountry(Long userId);

    Currency getCurrency(Long userId);

    UserZuora existUserZuora(Long userId) throws NotFoundExternalException;

    InsertUserResponse insertUserZuora(InsertUserRequest zuora);

    List<SubscriptionInfoResponse> renewSubscriptionsZuora(List<RenewSubscriptionsRequest> subscriptionsRequest) throws NotFoundExternalException;

    RefundPaymentsResponse refundPaymentsZuora(List<RefundPaymentsRequest> refundPaymentsRequest) throws NotFoundExternalException;

    UpdateInvoiceResponse updateInvoiceZuora(UpdateInvoiceRequest uidata);

    List<SubscriptionInfoResponse> cancelSubscriptionsZuora(List<CancelSubscriptionsRequest> subscriptionsRequest) throws NotFoundExternalException;
    List<SubscriptionInfoResponse> renewPaymentsZuora(List<RenewPaymentRequest> renewPaymentRequests ) throws NotFoundExternalException;
    List<SubscriptionInfoResponse> refundSubscriptionsZuora(List<RefundPaymentsRequest> refundPaymentsRequests) throws NotFoundExternalException;

    UserToPremiumResponse userToPremium(UserToPremiumRequest userToPremiumRequest);
    InsertUserDataResponse insertUserData(InsertUserDataRequest insertUserDataRequest);

    UpdateRenewalsCancelsResponse updateRenewalsCancels(final UpdateRenewalsCancelsRequest updateRenewalsCancelsRequest) throws ExternalException;

}

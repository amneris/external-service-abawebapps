package com.abaenglish.external.abawebapps.service.impl;

import com.abaenglish.external.abawebapps.domain.*;
import com.abaenglish.external.abawebapps.exception.BadRequestExternalException;
import com.abaenglish.external.abawebapps.exception.ExternalException;
import com.abaenglish.external.abawebapps.exception.NotFoundExternalException;
import com.abaenglish.external.abawebapps.exception.UnKnownException;
import com.abaenglish.external.abawebapps.service.IAbawebappsExternalService;
import com.abaenglish.external.abawebapps.service.properties.AbawebappsProperties;
import com.abaenglish.external.abawebapps.utils.HeaderInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class AbawebappsExternalService implements IAbawebappsExternalService {

    @Autowired
    private AbawebappsProperties abawebappsProperties;

    private RestTemplate restTemplate = new RestTemplate();

    @PostConstruct
    public void addInterceptor() {
        restTemplate.setInterceptors(Arrays.asList(
                new HeaderInterceptor(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE),
                new HeaderInterceptor(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)));

        restTemplate.setMessageConverters(getMessageConverters());
    }

    private List<HttpMessageConverter<?>> getMessageConverters() {
        List<HttpMessageConverter<?>> converters =
                new ArrayList<>();
        converters.add(new MappingJackson2HttpMessageConverter());
        return converters;
    }

    @Override
    public UpdateRenewalsCancelsResponse updateRenewalsCancels(final UpdateRenewalsCancelsRequest updateRenewalsCancelsRequest) throws ExternalException {

        UpdateRenewalsCancelsResponse updateRenewalsCancelsResponse = restTemplate.postForObject(
                abawebappsProperties.getUpdateRenewalsCancels(),
                updateRenewalsCancelsRequest,
                UpdateRenewalsCancelsResponse.class);

        switch (updateRenewalsCancelsResponse.getHttpCode()) {
            case 404:
                throw new NotFoundExternalException(new HttpClientErrorException(HttpStatus.NOT_FOUND, updateRenewalsCancelsResponse.getMessage()));
            case 200:
                break;
            default:
                throw new UnKnownException(new HttpClientErrorException(HttpStatus.valueOf(updateRenewalsCancelsResponse.getHttpCode()), updateRenewalsCancelsResponse.getMessage()));
        }

        return updateRenewalsCancelsResponse;

    }

    @Override
    public CurrencyISO getCurrencyISO(String iso) throws ExternalException {

        CurrencyISOResponse currencyISOResponse = restTemplate.getForObject(abawebappsProperties.getGetCurrencyISO() + iso, CurrencyISOResponse.class);
        CurrencyISO currencyISO = currencyISOResponse.getCurrencyISO();

        switch (currencyISOResponse.getHttpCode()) {
            case 400:
                throw new BadRequestExternalException(new HttpClientErrorException(HttpStatus.BAD_REQUEST, currencyISOResponse.getMessage()));
            case 404:
                throw new NotFoundExternalException(new HttpClientErrorException(HttpStatus.NOT_FOUND, currencyISOResponse.getMessage()));
            case 200:
                break;
            default:
                throw new UnKnownException(new HttpClientErrorException(HttpStatus.valueOf(currencyISOResponse.getHttpCode()), currencyISOResponse.getMessage()));
        }

        return currencyISO;

    }

    @Override
    public Currency getCurrency(Long userId) {
        return restTemplate.getForObject(abawebappsProperties.getUricurrency() + userId, Currency.class);
    }

    @Override
    public CountryIso getCountry(Long userId) {
        return restTemplate.getForObject(abawebappsProperties.getUricountry() + userId, CountryIso.class);
    }

    @Override
    public UserZuora existUserZuora(Long userId) throws NotFoundExternalException {

        UserZuora user;
        try {
            user = restTemplate.getForObject(abawebappsProperties.getExistUser() + userId, UserZuora.class);
        } catch (HttpClientErrorException e) {
            throw new NotFoundExternalException(e);
        }
        return user;

    }

    @Override
    public InsertUserResponse insertUserZuora(InsertUserRequest zuora) {

        return restTemplate.postForObject(
                abawebappsProperties.getInsertUser(),
                zuora,
                InsertUserResponse.class);
    }

    @Override
    public List<SubscriptionInfoResponse> cancelSubscriptionsZuora(List<CancelSubscriptionsRequest> subscriptionsRequest) throws NotFoundExternalException {

        SubscriptionInfoResponse[] response = restTemplate.postForObject(
                abawebappsProperties.getCancelSubscriptions(),
                subscriptionsRequest,
                SubscriptionInfoResponse[].class);
        return Arrays.asList(response);
    }

    @Override
    public List<SubscriptionInfoResponse> renewSubscriptionsZuora(List<RenewSubscriptionsRequest> subscriptionsRequest) throws NotFoundExternalException {

        SubscriptionInfoResponse[] response = restTemplate.postForObject(
                abawebappsProperties.getRenewSubscriptions(),
                subscriptionsRequest,
                SubscriptionInfoResponse[].class);
        return Arrays.asList(response);
    }

    @Override
    public List<SubscriptionInfoResponse> renewPaymentsZuora(List<RenewPaymentRequest> renewPaymentRequests) throws NotFoundExternalException {

        SubscriptionInfoResponse[] response = restTemplate.postForObject(
                abawebappsProperties.getRenewPayments(),
                renewPaymentRequests,
                SubscriptionInfoResponse[].class);

        return Arrays.asList(response);
    }

    @Override
    public List<SubscriptionInfoResponse> refundSubscriptionsZuora(List<RefundPaymentsRequest> refundPaymentsRequests) throws NotFoundExternalException {

        SubscriptionInfoResponse[] response = restTemplate.postForObject(
                abawebappsProperties.getRefundPayments(),
                refundPaymentsRequests,
                SubscriptionInfoResponse[].class);
        return Arrays.asList(response);
    }

    @Override
    public RefundPaymentsResponse refundPaymentsZuora(List<RefundPaymentsRequest> refundPaymentsRequest) throws NotFoundExternalException {

        return restTemplate.postForObject(
                abawebappsProperties.getRefundPayments(),
                refundPaymentsRequest,
                RefundPaymentsResponse.class);

    }

    @Override
    public UpdateInvoiceResponse updateInvoiceZuora(UpdateInvoiceRequest uidata) {
        return restTemplate.postForObject(
                abawebappsProperties.getUpdateInvoice(),
                uidata,
                UpdateInvoiceResponse.class);

    }

    @Override
    public UserToPremiumResponse userToPremium(UserToPremiumRequest userToPremiumRequest) {

        return restTemplate.postForObject(
                abawebappsProperties.getUserToPremium(),
                userToPremiumRequest,
                UserToPremiumResponse.class);
    }

    @Override
    public InsertUserDataResponse insertUserData(InsertUserDataRequest userData) {

        return restTemplate.postForObject(
                abawebappsProperties.getInsertUserData(),
                userData,
                InsertUserDataResponse.class);
    }

}

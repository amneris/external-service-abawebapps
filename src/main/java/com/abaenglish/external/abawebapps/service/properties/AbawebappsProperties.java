package com.abaenglish.external.abawebapps.service.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "external.abawebapps")
public class AbawebappsProperties {

    private String uricurrency;
    private String uricountry;
    private String existUser;
    private String insertUser;
    private String cancelSubscriptions;
    private String renewSubscriptions;
    private String renewPayments;
    private String refundPayments;
    private String updateInvoice;
    private String getCurrencyISO;
    private String userToPremium;
    private String insertUserData;
    private String updateRenewalsCancels;

    public String getUpdateRenewalsCancels() {
        return updateRenewalsCancels;
    }

    public void setUpdateRenewalsCancels(String updateRenewalsCancels) {
        this.updateRenewalsCancels = updateRenewalsCancels;
    }

    public String getGetCurrencyISO() {
        return getCurrencyISO;
    }

    public void setGetCurrencyISO(String getCurrencyISO) {
        this.getCurrencyISO = getCurrencyISO;
    }

    public String getUricurrency() {
        return uricurrency;
    }

    public void setUricurrency(String uricurrency) {
        this.uricurrency = uricurrency;
    }

    public String getUricountry() {
        return uricountry;
    }

    public void setUricountry(String uricountry) {
        this.uricountry = uricountry;
    }

    public String getExistUser() {
        return existUser;
    }

    public void setExistUser(String existUser) {
        this.existUser = existUser;
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser;
    }

    public String getCancelSubscriptions() {
        return cancelSubscriptions;
    }

    public void setCancelSubscriptions(String cancelSubscriptions) {
        this.cancelSubscriptions = cancelSubscriptions;
    }

    public String getRenewSubscriptions() {
        return renewSubscriptions;
    }

    public void setRenewSubscriptions(String renewSubscriptions) {
        this.renewSubscriptions = renewSubscriptions;
    }

    public String getRefundPayments() {
        return refundPayments;
    }

    public void setRefundPayments(String refundPayments) {
        this.refundPayments = refundPayments;
    }

    public String getUpdateInvoice() {
        return updateInvoice;
    }

    public void setUpdateInvoice(String updateInvoice) {
        this.updateInvoice = updateInvoice;
    }

    public String getRenewPayments() {
        return renewPayments;
    }

    public void setRenewPayments(String renewPayments) {
        this.renewPayments = renewPayments;
    }

    public String getUserToPremium() {
        return userToPremium;
    }

    public void setUserToPremium(String userToPremium) {
        this.userToPremium = userToPremium;
    }

    public String getInsertUserData() {
        return insertUserData;
    }

    public void setInsertUserData(String insertUserData) {
        this.insertUserData = insertUserData;
    }

}

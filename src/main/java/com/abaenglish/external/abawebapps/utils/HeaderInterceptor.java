package com.abaenglish.external.abawebapps.utils;

import com.abaenglish.boot.httprequest.BasicAuthInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.util.Arrays;

public class HeaderInterceptor implements ClientHttpRequestInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(BasicAuthInterceptor.class);
    private String key;
    private String value;

    public HeaderInterceptor(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        request.getHeaders().put(key, Arrays.asList(value));
        LOGGER.trace("Added header '{}':'{}'", key, value);
        return execution.execute(request, body);
    }
}

package com.abaenglish.external.abaweabpps.objectmother;

import com.abaenglish.external.abawebapps.domain.CurrencyISO;
import com.abaenglish.external.abawebapps.domain.CurrencyISOResponse;

public class MiscellaneousObjectMother {

    public static CurrencyISOResponse getCurrencyISOResponce(){

        CurrencyISO currencyISO = CurrencyISO.Builder.aCurrencyISO()
                .extCode("EUR")
                .description("Euro")
                .symbol("€")
                .unitEur("1.0000000000")
                .outEur("1.0000000000")
                .unitUsd("0.8828000000")
                .outUsd("1.1328000000")
                .decimalPoint(",")
                .dateLastUpdate("2015-10-19 22:15:02")
                .id("EUR")
                .build();

        CurrencyISOResponse currencyISOResponse = CurrencyISOResponse.Builder.aCurrencyISOResponse()
                .httpCode(200)
                .message("")
                .currencyISO(currencyISO)
                .build();

        return currencyISOResponse;

    }

    public static CurrencyISOResponse getCurrencyISOResponce404(){

        CurrencyISOResponse currencyISOResponse = CurrencyISOResponse.Builder.aCurrencyISOResponse()
                .httpCode(404)
                .message("Currency not found")
                .currencyISO(null)
                .build();

        return currencyISOResponse;

    }

    public static CurrencyISOResponse getCurrencyISOResponce400(){

        CurrencyISOResponse currencyISOResponse = CurrencyISOResponse.Builder.aCurrencyISOResponse()
                .httpCode(400)
                .message("Parameter ISO not found")
                .currencyISO(null)
                .build();

        return currencyISOResponse;

    }

    public static CurrencyISOResponse getCurrencyISOResponce405(){

        CurrencyISOResponse currencyISOResponse = CurrencyISOResponse.Builder.aCurrencyISOResponse()
                .httpCode(405)
                .message("")
                .currencyISO(null)
                .build();

        return currencyISOResponse;

    }

}

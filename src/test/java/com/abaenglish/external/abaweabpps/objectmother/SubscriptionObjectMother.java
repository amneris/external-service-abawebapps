package com.abaenglish.external.abaweabpps.objectmother;

import com.abaenglish.external.abawebapps.domain.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SubscriptionObjectMother {

    public static UserZuora userWithZuoraAccount() {

        User user = User.Builder.anUser()
                .id(265L)
                .country("MEX")
                .currency("MXN")
                .name("Name")
                .surname("Surname")
                .email("student+265@abaenglish.com")
                .userType(User.UserType.FREE)
                .build();

        Zuora zuora = Zuora.Builder.aZuora()
                .dateAdd(LocalDate.now().toString())
                .status("1")
                .zuoraAccountId("123456")
                .zuoraAccountNumber("123456789")
                .build();

        UserZuora userZuora = UserZuora.Builder.anUserZuora()
                .zuora(zuora)
                .user(user)
                .build();
        return userZuora;
    }

    public static InsertUserRequest requestInsertUser() {

        List<SubscriptionAbawebappsInfo> subscriptionsInfo = new ArrayList<>();

        SubscriptionAbawebappsInfo subscriptionAbawebappsInfo = new SubscriptionAbawebappsInfo();
        subscriptionAbawebappsInfo.setId("2c92c0f956b0ef040156b6d2865d1103");
        subscriptionAbawebappsInfo.setAccountId("2c92c0f856b0dd7b0156b2a1b3981b53");
        subscriptionAbawebappsInfo.setTermEndDate("2017-02-22");
        subscriptionAbawebappsInfo.setTermStartDate("2016-08-22");
        subscriptionsInfo.add(subscriptionAbawebappsInfo);

        InsertUserRequest insertUserRequest = InsertUserRequest.Builder.anInsertUserRequest()
                .userId(9338393L)
                .zuoraAccountId("123456789")
                .zuoraAccountNumber("12345AAA")

                .periodId(12)
                .currency("EUR")
                .price(new BigDecimal("199.99"))
                .idCountry("ESP")
                .idDiscount("2c92c0f9555351c1015558ac836f6b26")
                .idRatePlanCharge("2c92c0f8552e5302015530abaa915b5c")
                .invoiceId("2c92c0f95696fb2401569deb53bf0e67")
                .invoiceNumber("INV00000699")
                .finalPrice(new BigDecimal("69.99"))

                .subscriptionId("2c92c0f956b0ef040156b6d2865d1103")
                .subscriptionNumber("SU-0000000001")
                .subscriptionsList(subscriptionsInfo)

                .build();

        return insertUserRequest;
    }

    public static InsertUserResponse responseInsertUser() {
        InsertUserResponse insertUserResponse = InsertUserResponse.Builder.anInsertUserResponse()
                .success(true)
                .successFreeToPremium(true)
//                .paymentId("412de8fd")
                .build();
        return insertUserResponse;
    }

    public static List<CancelSubscriptionsRequest> requestCancelSubscriptions() {

        List<CancelSubscriptionsRequest> cancelSubscriptionsRequests = new ArrayList<>();

        CancelSubscriptionsRequest cancelSubscriptionsRequest  = new CancelSubscriptionsRequest();
        cancelSubscriptionsRequest.setOriginalSubscriptionId("2c92c0f956b0ef040156b6d2865d1103");
        cancelSubscriptionsRequest.setAccountId("2c92c0f856b0dd7b0156b2a1b3981b53");
        cancelSubscriptionsRequest.setCancelledDate("2017-02-22");
        cancelSubscriptionsRequests.add(cancelSubscriptionsRequest);

        CancelSubscriptionsRequest cancelSubscriptionsRequest1 = new CancelSubscriptionsRequest();
        cancelSubscriptionsRequest1.setOriginalSubscriptionId("FAKE-2c92c0f956b0ef040156b6d2865d1103");
        cancelSubscriptionsRequest1.setAccountId("FAKE-2c92c0f856b0dd7b0156b2a1b3981b53");
        cancelSubscriptionsRequest1.setCancelledDate("FAKE-2017-02-22");

        cancelSubscriptionsRequests.add(cancelSubscriptionsRequest1);

        return cancelSubscriptionsRequests;
    }

    public static List<SubscriptionInfoResponse> responseCancelSubscriptions() {

        SubscriptionInfoResponse subscriptionInfoResponse = SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                .originalSubscriptionId("2c92c0f956b0ef040156b6d2865d1103")
                .success(true)
                .build();

        SubscriptionInfoResponse subscriptionInfoResponse1 = SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                .originalSubscriptionId("FAKE-2c92c0f956b0ef040156b6d2865d1103")
                .success(false)
                .build();

        return Arrays.asList(subscriptionInfoResponse, subscriptionInfoResponse1);
    }

    public static List<RenewSubscriptionsRequest> requestRenewSubscriptions() {

        List<RenewSubscriptionsRequest> renewSubscriptionsRequests = new ArrayList<>();

        RenewSubscriptionsRequest renewSubscriptionsRequest = new RenewSubscriptionsRequest();
        renewSubscriptionsRequest.setOriginalSubscriptionId("2c92c0f956b0ef040156b6d2865d1103");
        renewSubscriptionsRequest.setUserExpirationDate("2017-02-22");
        renewSubscriptionsRequests.add(renewSubscriptionsRequest);

        RenewSubscriptionsRequest renewSubscriptionsRequest1 = new RenewSubscriptionsRequest();
        renewSubscriptionsRequest1.setOriginalSubscriptionId("FAKE-2c92c0f956b0ef040156b6d2865d1103");
        renewSubscriptionsRequest1.setUserExpirationDate("2017-02-22");
        renewSubscriptionsRequests.add(renewSubscriptionsRequest1);

        return renewSubscriptionsRequests;
    }

    public static List<SubscriptionInfoResponse> responseRenewSubscriptions() {

        SubscriptionInfoResponse subscriptionInfoResponse = SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                .originalSubscriptionId("2c92c0f956b0ef040156b6d2865d1103")
                .success(true)
                .build();

        SubscriptionInfoResponse subscriptionInfoResponse1 = SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                .originalSubscriptionId("FAKE-2c92c0f956b0ef040156b6d2865d1103")
                .success(false)
                .build();

        return Arrays.asList(subscriptionInfoResponse, subscriptionInfoResponse1);
    }

    public static List<RefundPaymentsRequest> requestRefundPayments() {

        List<RefundPaymentsRequest> refundPaymentsRequests = new ArrayList<>();

        RefundPaymentsRequest refundPaymentsRequest = new RefundPaymentsRequest();
        refundPaymentsRequest.setId("2c92c0f956b0ef040156b6d2865d1103");
        refundPaymentsRequest.setAccountId("2c92c0f856b0dd7b0156b2a1b3981b53");
        refundPaymentsRequest.setAmount(new BigDecimal("29.99"));
        refundPaymentsRequest.setRefundDate("2016-09-04");
        refundPaymentsRequests.add(refundPaymentsRequest);

        RefundPaymentsRequest refundPaymentsRequest1 = new RefundPaymentsRequest();
        refundPaymentsRequest1.setId("FAKE-2c92c0f956b0ef040156b6d2865d1103");
        refundPaymentsRequest1.setAccountId("FAKE-2c92c0f856b0dd7b0156b2a1b3981b53");
        refundPaymentsRequest1.setAmount(new BigDecimal("29.99"));
        refundPaymentsRequest1.setRefundDate("2016-09-04");
        refundPaymentsRequests.add(refundPaymentsRequest1);

        return refundPaymentsRequests;
    }

    public static RefundPaymentsResponse refundPaymentsResponse() {

        List<SubscriptionInfoResponse> success = new ArrayList<>();

        SubscriptionInfoResponse subscriptionInfoResponse = SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                .originalSubscriptionId("2c92c0f956b0ef040156b6d2865d1103")
                .success(true)
                .build();

        success.add(subscriptionInfoResponse);

        SubscriptionInfoResponse subscriptionInfoResponse1 = SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                .originalSubscriptionId("FAKE-2c92c0f956b0ef040156b6d2865d1103")
                .success(false)
                .build();

        success.add(subscriptionInfoResponse1);

        RefundPaymentsResponse refundPaymentsResponse = RefundPaymentsResponse.Builder.aRefundPaymentsResponse()
                .success(success)
                .build();

        return refundPaymentsResponse;
    }


    public static UserToPremiumResponse responseUserToPremium() {

        UserToPremiumResponse userToPremiumResponse = UserToPremiumResponse.Builder.anUserToPremiumResponse()
                .httpCode(200)
                .message("")
                .build();

        return userToPremiumResponse;
    }

    public static UserToPremiumResponse responseUserToPremium202UserAlreadyExists() {

        UserToPremiumResponse userToPremiumResponse = UserToPremiumResponse.Builder.anUserToPremiumResponse()
                .httpCode(200)
                .message("User already exists and is premium!")
                .build();

        return userToPremiumResponse;
    }

    public static UserToPremiumResponse responseUserToPremium404UserDoesNotExists() {

        UserToPremiumResponse userToPremiumResponse = UserToPremiumResponse.Builder.anUserToPremiumResponse()
                .httpCode(404)
                .message("User does not exist!")
                .build();

        return userToPremiumResponse;
    }

    public static UserToPremiumRequest requestUserToPremium() {

        UserToPremiumRequest userToPremiumRequest = UserToPremiumRequest.Builder.anUserToPremiumRequest()
                .userId(9337820L)
                .expirationDate("2018-03-20 00:11:22")
                .build();

        return userToPremiumRequest;
    }

    public static UserToPremiumRequest requestUserToPremium404UserDoesNotExists() {

        UserToPremiumRequest userToPremiumRequest = UserToPremiumRequest.Builder.anUserToPremiumRequest()
                .userId(1L)
                .expirationDate("2018-03-20")
                .build();

        return userToPremiumRequest;
    }

    /**
     * @return
     */
    public static InsertUserDataResponse responseInsertUserData() {
        InsertUserDataResponse insertUserDataResponse = InsertUserDataResponse.Builder.anInsertUserDataResponse()
                .httpCode(200)
                .httpCodePayment(200)
                .httpCodeSubscription(200)
                .message("")
                .build();
        return insertUserDataResponse;
    }

    /**
     * @return
     */
    public static InsertUserDataResponse responseInsertUserData400UserNotFound() {
        InsertUserDataResponse insertUserDataResponse = InsertUserDataResponse.Builder.anInsertUserDataResponse()
                .httpCode(404)
                .message("User not found!")
                .build();
        return insertUserDataResponse;
    }

    /**
     * @return
     */
    public static InsertUserDataRequest requestInsertUserData() {

        List<SubscriptionAbawebappsInfo> subscriptionsInfo = new ArrayList<>();

        SubscriptionAbawebappsInfo subscriptionAbawebappsInfo = new SubscriptionAbawebappsInfo();
        subscriptionAbawebappsInfo.setId("2c92c0f956b0ef040156b6d2865d1103");
        subscriptionAbawebappsInfo.setAccountId("2c92c0f856b0dd7b0156b2a1b3981b53");
        subscriptionAbawebappsInfo.setTermEndDate("2018-03-23");
        subscriptionAbawebappsInfo.setTermStartDate("2017-03-23");
        subscriptionsInfo.add(subscriptionAbawebappsInfo);

        InsertUserDataRequest insertUserDataRequest = InsertUserDataRequest.Builder.anInsertUserDataRequest()
                .userId(9340246L)
                .zuoraAccountId("123456789")
                .zuoraAccountNumber("12345AAA")
                .periodId(12)
                .currency("EUR")
                .price(new BigDecimal("199.99"))
                .idCountry("ESP")
                .idDiscount("2c92c0f9555351c1015558ac836f6b26")
                .invoiceId("2c92c0f95696fb2401569deb53bf0e67")
                .invoiceNumber("INV00000699")
                .finalPrice(new BigDecimal("69.99"))
                .subscriptionId("2c92c0f956b0ef040156b6d2865d1103")
                .subscriptionNumber("SU-0000000001")
                .subscriptionsList(subscriptionsInfo)
                .build();

        return insertUserDataRequest;
    }

}

package com.abaenglish.external.abaweabpps.exception;

import com.abaenglish.external.abawebapps.exception.NotFoundExternalException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.HttpStatus;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;

public class NotFoundExternalExceptionTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testNotFoundException() throws NotFoundExternalException {

        thrown.expect(NotFoundExternalException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.NOT_FOUND)));
        executeException();
    }

    private void executeException() throws NotFoundExternalException {
        try {
            List<String> list = null;
            String item = list.get(0);
        } catch (Exception e) {
            throw new NotFoundExternalException(e);
        }
    }
}

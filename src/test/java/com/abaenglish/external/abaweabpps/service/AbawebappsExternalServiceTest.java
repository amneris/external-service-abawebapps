package com.abaenglish.external.abaweabpps.service;

import com.abaenglish.external.abaweabpps.objectmother.MiscellaneousObjectMother;
import com.abaenglish.external.abaweabpps.objectmother.SubscriptionObjectMother;
import com.abaenglish.external.abawebapps.domain.*;
import com.abaenglish.external.abawebapps.exception.BadRequestExternalException;
import com.abaenglish.external.abawebapps.exception.ExternalException;
import com.abaenglish.external.abawebapps.exception.NotFoundExternalException;
import com.abaenglish.external.abawebapps.exception.UnKnownException;
import com.abaenglish.external.abawebapps.service.impl.AbawebappsExternalService;
import com.abaenglish.external.abawebapps.service.properties.AbawebappsProperties;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;

// TODO rehacer todos los test
public class AbawebappsExternalServiceTest {

    @InjectMocks
    private AbawebappsExternalService abawebappsExternalService;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private AbawebappsProperties abawebappsProperties;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getCurrencyISO() {
        final CurrencyISOResponse motherCurrencyISOResponse = MiscellaneousObjectMother.getCurrencyISOResponce();

        Mockito.when(abawebappsProperties.getGetCurrencyISO()).thenReturn("http://api-int.abaenglish.com/api/apiuser/getcurrency/");
        Mockito.when(restTemplate.getForObject("http://api-int.abaenglish.com/api/apiuser/getcurrency/eur", CurrencyISOResponse.class)).thenReturn(motherCurrencyISOResponse);

        CurrencyISO currencyISO = null;

        try{
            currencyISO = abawebappsExternalService.getCurrencyISO("eur");
        } catch (BadRequestExternalException e) {
        } catch (NotFoundExternalException e) {
        } catch (ExternalException e) {
        }


        assertNotNull(currencyISO);
        AssertionsForClassTypes.assertThat(currencyISO).isEqualTo(motherCurrencyISOResponse.getCurrencyISO());

    }

    @Test
    public void getCurrencyISO_404() {
        final CurrencyISOResponse motherCurrencyISOResponse = MiscellaneousObjectMother.getCurrencyISOResponce404();

        Mockito.when(abawebappsProperties.getGetCurrencyISO()).thenReturn("http://api-int.abaenglish.com/api/apiuser/getcurrency/");
        Mockito.when(restTemplate.getForObject("http://api-int.abaenglish.com/api/apiuser/getcurrency/eur", CurrencyISOResponse.class)).thenReturn(motherCurrencyISOResponse);

        CurrencyISO currencyISO = null;

        try{
            currencyISO = abawebappsExternalService.getCurrencyISO("eur");
        } catch (BadRequestExternalException e) {
            assertEquals(1, 0);
        } catch (NotFoundExternalException e) {
            assertEquals(e.getHttpStatus().value(), 404);
        } catch (ExternalException e) {
            assertEquals(1, 0);
        }

        assertNull(currencyISO);

    }

    @Test
    public void getCurrencyISO_400() {
        final CurrencyISOResponse motherCurrencyISOResponse = MiscellaneousObjectMother.getCurrencyISOResponce400();

        Mockito.when(abawebappsProperties.getGetCurrencyISO()).thenReturn("http://api-int.abaenglish.com/api/apiuser/getcurrency/");
        Mockito.when(restTemplate.getForObject("http://api-int.abaenglish.com/api/apiuser/getcurrency/eur", CurrencyISOResponse.class)).thenReturn(motherCurrencyISOResponse);

        CurrencyISO currencyISO = null;

        try{
            currencyISO = abawebappsExternalService.getCurrencyISO("eur");
        } catch (BadRequestExternalException e) {
            assertEquals(e.getHttpStatus().value(), 400);
        } catch (NotFoundExternalException e) {
            assertEquals(1, 0);
        } catch (ExternalException e) {
            assertEquals(1, 0);
        }

        assertNull(currencyISO);

    }
    @Test
    public void getCurrencyISO_405() {
        final CurrencyISOResponse motherCurrencyISOResponse = MiscellaneousObjectMother.getCurrencyISOResponce405();

        Mockito.when(abawebappsProperties.getGetCurrencyISO()).thenReturn("http://api-int.abaenglish.com/api/apiuser/getcurrency/");
        Mockito.when(restTemplate.getForObject("http://api-int.abaenglish.com/api/apiuser/getcurrency/eur", CurrencyISOResponse.class)).thenReturn(motherCurrencyISOResponse);

        CurrencyISO currencyISO = null;

        try{
            currencyISO = abawebappsExternalService.getCurrencyISO("eur");
        } catch (BadRequestExternalException e) {
            assertEquals(1, 0);
        } catch (NotFoundExternalException e) {
            assertEquals(1, 0);
        } catch (UnKnownException e) {
            assertEquals(e.getMessage(), "org.springframework.web.client.HttpClientErrorException: 405 ");
        } catch (ExternalException e) {
            assertEquals(1, 0);
        }

        assertNull(currencyISO);

    }

    @Test
    public void getCountry() {
        final CountryIso countryIso = CountryIso.Builder.aCountryIso()
                .abaCountryIso("199")
                .idCountry(1L)
                .build();

        Mockito.when(abawebappsProperties.getUricountry()).thenReturn("http://api-int.abaenglish.com/api/apiuser/getcountrybyuserid/");
        Mockito.when(restTemplate.getForObject("http://api-int.abaenglish.com/api/apiuser/getcountrybyuserid/404", CountryIso.class)).thenReturn(countryIso);

        CountryIso country = abawebappsExternalService.getCountry(404L);
        assertNotNull(country);
        AssertionsForClassTypes.assertThat(countryIso.getAbaCountryIso()).isEqualTo(country.getAbaCountryIso());
        AssertionsForClassTypes.assertThat(countryIso.getIdCountry()).isEqualTo(country.getIdCountry());

    }

    @Test
    public void getCurrency() {

        //TODO: Refactor Spring 4.3 http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/test/web/client/MockRestServiceServer.html

        final Currency currency = Currency.Builder.aCurrency()
                .abaIdCurrency("USD")
                .build();

        Mockito.when(abawebappsProperties.getUricurrency()).thenReturn("http://api-int.abaenglish.com/api/apiuser/getcurrencybyuserid/");
        Mockito.when(restTemplate.getForObject("http://api-int.abaenglish.com/api/apiuser/getcurrencybyuserid/666", Currency.class)).thenReturn(currency);

        Currency testCurrency = abawebappsExternalService.getCurrency(666L);

        assertNotNull(testCurrency);
        assertEquals(testCurrency.getAbaIdCurrency(), "USD");

    }

    @Test
    public void existUserZuora() throws NotFoundExternalException {

        UserZuora response = SubscriptionObjectMother.userWithZuoraAccount();

        Mockito.when(abawebappsExternalService.existUserZuora(265L)).thenReturn(response);

        UserZuora test = abawebappsExternalService.existUserZuora(265L);

        assertNotNull(test);
        assertEquals(test.getUser().getCurrency(), "MXN");
        assertEquals(test.getUser().getName(), "Name");
        assertEquals(test.getUser().getUserType(), User.UserType.FREE);
    }

    @Test
    public void insertUserResponse() {
        InsertUserResponse response = SubscriptionObjectMother.responseInsertUser();

        InsertUserRequest request = SubscriptionObjectMother.requestInsertUser();

        Mockito.when(abawebappsExternalService.insertUserZuora(request)).thenReturn(response);

        InsertUserResponse responseTest = abawebappsExternalService.insertUserZuora(request);

        assertNotNull(responseTest);
        assertEquals(responseTest.getSuccess(), true);
        assertEquals(responseTest.getSuccessFreeToPremium(), true);
    }

//TODO update test
//    @Test
//    public void cancelSubscriptionsResponse() throws NotFoundExternalException {
//
//        List<CancelSubscriptionsRequest> cancelSubscriptionsRequest = SubscriptionObjectMother.requestCancelSubscriptions();
//        CancelSubscriptionsResponse cancelSubscriptionsResponse = SubscriptionObjectMother.responseCancelSubscriptions();
//
//        Mockito.when(abawebappsExternalService.cancelSubscriptionsZuora(cancelSubscriptionsRequest)).thenReturn(cancelSubscriptionsResponse);
//
//        CancelSubscriptionsResponse responseTest = abawebappsExternalService.cancelSubscriptionsZuora(cancelSubscriptionsRequest);
//
//        assertNotNull(responseTest);
////        assertEquals(responseTest.getSuccess(), cancelSubscriptionsResponse.getSuccess());
//    }


    @Test
    public void refundPaymentsResponse() throws NotFoundExternalException {

        List<RefundPaymentsRequest> refundPaymentsRequest = SubscriptionObjectMother.requestRefundPayments();
        RefundPaymentsResponse refundPaymentsResponse = SubscriptionObjectMother.refundPaymentsResponse();

        Mockito.when(abawebappsExternalService.refundPaymentsZuora(refundPaymentsRequest)).thenReturn(refundPaymentsResponse);

        RefundPaymentsResponse responseTest = abawebappsExternalService.refundPaymentsZuora(refundPaymentsRequest);

        assertNotNull(responseTest);
//        assertEquals(responseTest.getSuccess(), refundPaymentsResponse.getSuccess());
    }


    @Test
    public void testUpdateToPremiumTest() {

        UserToPremiumResponse response = SubscriptionObjectMother.responseUserToPremium();
        UserToPremiumRequest request = SubscriptionObjectMother.requestUserToPremium();

        Mockito.when(abawebappsExternalService.userToPremium(request)).thenReturn(response);
        UserToPremiumResponse responseTest = abawebappsExternalService.userToPremium(request);

        assertNotNull(responseTest);
        assertEquals(responseTest.getHttpCode().intValue(), 200);
        assertEquals(responseTest.getMessage(), "");
    }

    @Test
    public void testUpdateToPremium404UserDoesNotExistsTest() {

        UserToPremiumResponse response = SubscriptionObjectMother.responseUserToPremium404UserDoesNotExists();
        UserToPremiumRequest request = SubscriptionObjectMother.requestUserToPremium404UserDoesNotExists();

        Mockito.when(abawebappsExternalService.userToPremium(request)).thenReturn(response);
        UserToPremiumResponse responseTest = abawebappsExternalService.userToPremium(request);

        assertNotNull(responseTest);
        assertEquals(responseTest.getHttpCode().intValue(), 404);
        assertEquals(responseTest.getMessage(), "User does not exist!");
    }

    @Test
    public void testUpdateToPremium202UserAlreadyExistsTest() {

        UserToPremiumResponse response = SubscriptionObjectMother.responseUserToPremium202UserAlreadyExists();
        UserToPremiumRequest request = SubscriptionObjectMother.requestUserToPremium();

        Mockito.when(abawebappsExternalService.userToPremium(request)).thenReturn(response);
        UserToPremiumResponse responseTest = abawebappsExternalService.userToPremium(request);

        assertNotNull(responseTest);
        assertEquals(responseTest.getHttpCode().intValue(), 200);
        assertEquals(responseTest.getMessage(), "User already exists and is premium!");
    }

    @Test
    public void testInsertUserData() {

        InsertUserDataResponse response = SubscriptionObjectMother.responseInsertUserData();
        InsertUserDataRequest request = SubscriptionObjectMother.requestInsertUserData();

        Mockito.when(abawebappsExternalService.insertUserData(request)).thenReturn(response);
        InsertUserDataResponse responseTest = abawebappsExternalService.insertUserData(request);

        assertNotNull(responseTest);
        assertEquals(responseTest.getHttpCode().intValue(), 200);
        assertEquals(responseTest.getHttpCodePayment().intValue(), 200);
        assertEquals(responseTest.getHttpCodeSubscription().intValue(), 200);
        assertEquals(responseTest.getMessage(), "");
    }

    @Test
    public void testInsertUserData404UserNotFound() {

        InsertUserDataResponse response = SubscriptionObjectMother.responseInsertUserData400UserNotFound();
        InsertUserDataRequest request = SubscriptionObjectMother.requestInsertUserData();

        Mockito.when(abawebappsExternalService.insertUserData(request)).thenReturn(response);
        InsertUserDataResponse responseTest = abawebappsExternalService.insertUserData(request);

        assertNotNull(responseTest);
        assertEquals(responseTest.getHttpCode().intValue(), 404);
        assertEquals(responseTest.getMessage(), "User not found!");
    }

}

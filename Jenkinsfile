#!/usr/bin/env groovy
try {

    stage 'build'
    node {
        deleteDir()
        checkout scm

        if ("${env.BRANCH_NAME}" != 'null') {
            // This is a multibranch pipeline, so we need to set the name of the branch
            sh "git checkout -b ${env.BRANCH_NAME}"
        }

        withEnv(["PATH+MAVEN=${tool 'maven3'}/bin"]) {
            sh "mvn -B clean verify -DskipTests"
        }

        stash excludes: 'target/', name: 'source'
    }

    stage 'tests'
    node {
        unstash 'source'

        withEnv(["PATH+MAVEN=${tool 'maven3'}/bin"]) {
            sh "mvn -B clean verify -Pcoverage"
        }
        step([$class: 'JUnitResultArchiver', testResults: '**/target/*-reports/*.xml'])
    }

    if ("${env.BRANCH_NAME}" != 'null' && "${env.BRANCH_NAME}".startsWith('PR-')) {
        // This is a multibranch pipeline, used for pullrequest, we need to launch code quality
        stage 'code quality'
        node {
            def pullRequest = "${env.BRANCH_NAME}".replace('PR-', '')
            unstash 'source'
            withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'sonarqube', passwordVariable: 'SONAR_PASSWORD', usernameVariable: 'SONAR_LOGIN']]) {
                withCredentials([[$class: 'StringBinding', credentialsId: '7cacb33c-5bfc-4d39-b678-a921023123d9', variable: 'GITHUB_ACCESS_TOKEN']]) {
                    withEnv(["PATH+MAVEN=${tool 'maven3'}/bin"]) {
                        sh "mvn -e -B sonar:sonar -Dsonar.host.url=http://sonar.aba.land:9000/ " +
                                "-Dsonar.branch=develop " +
                                "-Dsonar.login=${env.SONAR_LOGIN} " +
                                "-Dsonar.password=${env.SONAR_PASSWORD} " +
                                "-Dsonar.analysis.mode=preview " +
                                "-Dsonar.github.pullRequest=${pullRequest} " +
                                "-Dsonar.github.repository=abaenglish/external-service-abawebapps " +
                                "-Dsonar.github.oauth=${env.GITHUB_ACCESS_TOKEN}"
                    }
                }
            }
        }

        // This is a multibranch pipeline, used for pullrequest, we need to launch code review
        stage 'code review'
        node {
            unstash 'source'
            env.ghprbPullId = "${env.BRANCH_NAME}".replace('PR-', '')
            env.GIT_URL = "https://github.com/abaenglish/external-service-abawebapps.git"
            env.WORKSPACE = pwd()
            sh "docker pull rcruzper/danger:latest"
            withCredentials([[$class: 'StringBinding', credentialsId: '7cacb33c-5bfc-4d39-b678-a921023123d9', variable: 'DANGER_GITHUB_API_TOKEN']]) {
                sh "docker run -v \"${env.WORKSPACE}\":/usr/src/app -w /usr/src/app -e JENKINS_URL=${env.JENKINS_URL} -e GIT_URL=${env.GIT_URL} -e ghprbPullId=${env.ghprbPullId} -e DANGER_GITHUB_API_TOKEN=${env.DANGER_GITHUB_API_TOKEN} rcruzper/danger:latest"
            }
            slackSend channel: '#bazt', color: 'good', message: "External Service Abawebapps ${env.JOB_BASE_NAME} - ${env.BUILD_DISPLAY_NAME} Success (${env.BUILD_URL})"
        }
    } else if ("${env.BRANCH_NAME}".startsWith('develop')) {
        stage('code quality') {
            node {
                unstash 'source'

                withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'sonarqube', passwordVariable: 'SONAR_PASSWORD', usernameVariable: 'SONAR_LOGIN']]) {
                    withEnv(["PATH+MAVEN=${tool 'maven3'}/bin"]) {
                        sh "mvn -e -B sonar:sonar -Dsonar.host.url=http://sonar.aba.land:9000/ -Dsonar.branch=develop -Dsonar.login=${env.SONAR_LOGIN} -Dsonar.password=${env.SONAR_PASSWORD}"
                    }
                }
            }
        }

        stage('publish artifact') {
            node {
                unstash 'source'

                withEnv(["PATH+MAVEN=${tool 'maven3'}/bin"]) {
                    sh "mvn -B deploy -DskipTests"
                }
            }
        }
    }

} catch (err) {
    slackSend channel: '#bazt', color: 'danger', message: "External Service Abawebapps ${env.JOB_BASE_NAME} - ${env.BUILD_DISPLAY_NAME} Fail (${env.BUILD_URL})"
    throw err
}
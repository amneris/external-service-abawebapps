# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased
### Add
- Add new endpoint to update invoice data & payment amount in abawebapps
- Fix response of methods: cancelSubscriptionsZuora, renewSubscriptionsZuora, renewPaymentsZuora and refundSubscriptionsZuora
- Add new endpoint getCurrencyISO.
- Add new endpoint: userToPremium
- Add new endpoint: insertUserData
- Add httpCode to userToPremium and insertUserData endpoints response & resolve conflicts
- Remove frovider from UserToPremiumRequest
- Add new endpoint: updateRenewalsCancels

 [1.0.4]
### Changed
- Removed termStartDate and termEndDate in CancelSubscriptionsRequest and RenewSubscriptionsRequest
- Added paymentDate in RenewSubscriptionsRequest
- Added userExpirationDate to CancelSubscriptionsRequest
### Added
- Added field partnerId (from aba payment) in InsertUserResponse

## [1.0.3]
### Added
- Rename fields in SubscriptionAbawebappsInf